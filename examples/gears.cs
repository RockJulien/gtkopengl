/*
 * 3-D gear wheels.  This program is in the public domain.
 *
 * Brian Paul
 */

/* Conversion to GLUT by Mark J. Kilgard */

/* Conversion to GtkGLExt by Naofumi Yasufuku */

/*
 * gears.cs:
 * Conversion for GTK# - gtkgl-sharp.
 *
 * converted by Luciano Martorella  <mad_lux_it@users.sourceforge.net>
*/


using Gtk;
using GtkSharp;
using GdkGL;
using GtkGL;
using System;
using Tao.OpenGl;


namespace Gtk 
{
	class	GearsArea : DrawingArea
	{
		/*
		* Draw a gear wheel.  You'll probably want to call this function when
		* building a display list since we do a lot of trig here.
		*
		* Input:  inner_radius - radius of hole at center
		* outer_radius - radius at center of teeth
		* Width - Width of gear
		* teeth - number of teeth
		* tooth_depth - depth of tooth
		*/
		
		private void Gear (float inner_radius,
						    float outer_radius,
						    float Width,
						    int   teeth,
						    float tooth_depth)
		{
			float r0 = inner_radius;
			float r1 = outer_radius - tooth_depth / 2.0f;
			float r2 = outer_radius + tooth_depth / 2.0f;
			
			float da = (float)(2.0 * Math.PI / teeth / 4.0);
			
			Gl.glShadeModel (Gl.GL_FLAT);
			
			Gl.glNormal3f (0, 0, 1);
			
			/* draw front face */
			Gl.glBegin (Gl.GL_QUAD_STRIP);
			for (int i = 0; i <= teeth; i++) {
				double angle = i * 2.0 * Math.PI / teeth;
				Gl.glVertex3f ((float)(r0 * Math.Cos(angle)), (float)(r0 * Math.Sin(angle)), Width * 0.5f);
				Gl.glVertex3f ((float)(r1 * Math.Cos(angle)), (float)(r1 * Math.Sin(angle)), Width * 0.5f);
				if (i < teeth) {
				    Gl.glVertex3f ((float)(r0 * Math.Cos(angle)), 
									(float)(r0 * Math.Sin(angle)), 
									Width * 0.5f);
				    Gl.glVertex3f ((float)(r1 * Math.Cos(angle + 3 * da)), 
							       	(float)(r1 * Math.Sin(angle + 3 * da)), 
									Width * 0.5f);
				}
			}
			Gl.glEnd();
			
			/* draw front sides of teeth */
			Gl.glBegin (Gl.GL_QUADS);
			da = (float)(2.0 * Math.PI / teeth / 4.0);
			for (int i = 0; i < teeth; i++) {
				double angle = i * 2.0 * Math.PI / teeth;
				
				Gl.glVertex3f ((float)(r1 * Math.Cos(angle)), (float)(r1 * Math.Sin(angle)), Width * 0.5f);
				Gl.glVertex3f ((float)(r2 * Math.Cos(angle + da)), (float)(r2 * Math.Sin(angle + da)), Width * 0.5f);
				Gl.glVertex3f ((float)(r2 * Math.Cos(angle + 2 * da)), (float)(r2 * Math.Sin(angle + 2 * da)), Width * 0.5f);
				Gl.glVertex3f ((float)(r1 * Math.Cos(angle + 3 * da)), (float)(r1 * Math.Sin(angle + 3 * da)), Width * 0.5f);
			}
			Gl.glEnd();
			
			Gl.glNormal3f (0, 0, -1);
			
			/* draw back face */
			Gl.glBegin (Gl.GL_QUAD_STRIP);
			for (int i = 0; i <= teeth; i++) {
				double angle = i * 2.0 * Math.PI / teeth;
				Gl.glVertex3f ((float)(r1 * Math.Cos(angle)), (float)(r1 * Math.Sin(angle)), -Width * 0.5f);
				Gl.glVertex3f ((float)(r0 * Math.Cos(angle)), (float)(r0 * Math.Sin(angle)), -Width * 0.5f);
				if (i < teeth) {
				    Gl.glVertex3f ((float)(r1 * Math.Cos(angle + 3 * da)), 
									(float)(r1 * Math.Sin(angle + 3 * da)), 
									-Width * 0.5f);
				    Gl.glVertex3f ((float)(r0 * Math.Cos(angle)), 
							       	(float)(r0 * Math.Sin(angle)), 
									-Width * 0.5f);
				}
			}
			Gl.glEnd();
			
			/* draw back sides of teeth */
			Gl.glBegin (Gl.GL_QUADS);
			da = (float)(2.0 * Math.PI / teeth / 4.0);
			for (int i = 0; i < teeth; i++) {
				double angle = i * 2.0 * Math.PI / teeth;

				Gl.glVertex3f ((float)(r1 * Math.Cos(angle + 3 * da)), (float)(r1 * Math.Sin(angle + 3 * da)), -Width * 0.5f);
				Gl.glVertex3f ((float)(r2 * Math.Cos(angle + 2 * da)), (float)(r2 * Math.Sin(angle + 2 * da)), -Width * 0.5f);
				Gl.glVertex3f ((float)(r2 * Math.Cos(angle + da)), (float)(r2 * Math.Sin(angle + da)), -Width * 0.5f);
				Gl.glVertex3f ((float)(r1 * Math.Cos(angle)), (float)(r1 * Math.Sin(angle)), -Width * 0.5f);
			}
			Gl.glEnd();
			
			/* draw outward faces of teeth */
			Gl.glBegin (Gl.GL_QUAD_STRIP);
			for (int i = 0; i < teeth; i++) {
				double angle = i * 2.0 * Math.PI / teeth;
				
				Gl.glVertex3f ((float)(r1 * Math.Cos(angle)), (float)(r1 * Math.Sin(angle)), Width * 0.5f);
				Gl.glVertex3f ((float)(r1 * Math.Cos(angle)), (float)(r1 * Math.Sin(angle)), -Width * 0.5f);
				double u = r2 * Math.Cos(angle + da) - r1 * Math.Cos(angle);
				double v = r2 * Math.Sin(angle + da) - r1 * Math.Sin(angle);
				double len = Math.Sqrt (u * u + v * v);
				u /= len;
				v /= len;
				Gl.glNormal3f ((float)v, (float)-u, 0);
				Gl.glVertex3f ((float)(r2 * Math.Cos(angle + da)), (float)(r2 * Math.Sin(angle + da)), Width * 0.5f);
				Gl.glVertex3f ((float)(r2 * Math.Cos(angle + da)), (float)(r2 * Math.Sin(angle + da)), -Width * 0.5f);
				Gl.glNormal3f ((float)(Math.Cos(angle)), (float)(Math.Sin(angle)), 0);
				Gl.glVertex3f ((float)(r2 * Math.Cos(angle + 2 * da)), (float)(r2 * Math.Sin(angle + 2 * da)), Width * 0.5f);
				Gl.glVertex3f ((float)(r2 * Math.Cos(angle + 2 * da)), (float)(r2 * Math.Sin(angle + 2 * da)), -Width * 0.5f);
				u = r1 * Math.Cos(angle + 3 * da) - r2 * Math.Cos(angle + 2 * da);
				v = r1 * Math.Sin(angle + 3 * da) - r2 * Math.Sin(angle + 2 * da);
				Gl.glNormal3f ((float)v, (float)(-u), 0);
				Gl.glVertex3f ((float)(r1 * Math.Cos(angle + 3 * da)), (float)(r1 * Math.Sin(angle + 3 * da)), Width * 0.5f);
				Gl.glVertex3f ((float)(r1 * Math.Cos(angle + 3 * da)), (float)(r1 * Math.Sin(angle + 3 * da)), -Width * 0.5f);
				Gl.glNormal3f ((float)(Math.Cos(angle)), (float)(Math.Sin(angle)), 0);
			}
			
			Gl.glVertex3f ((float)(r1 * Math.Cos(0)), (float)(r1 * Math.Sin(0)), Width * 0.5f);
			Gl.glVertex3f ((float)(r1 * Math.Cos(0)), (float)(r1 * Math.Sin(0)), -Width * 0.5f);
			
			Gl.glEnd();
			
			Gl.glShadeModel (Gl.GL_SMOOTH);
			
			/* draw inside radius cylinder */
			Gl.glBegin (Gl.GL_QUAD_STRIP);
			for (int i = 0; i <= teeth; i++) {
				double angle = i * 2.0 * Math.PI / teeth;
				Gl.glNormal3f ((float)(-Math.Cos(angle)), (float)(-Math.Sin(angle)), 0);
				Gl.glVertex3f ((float)(r0 * Math.Cos(angle)), (float)(r0 * Math.Sin(angle)), -Width * 0.5f);
				Gl.glVertex3f ((float)(r0 * Math.Cos(angle)), (float)(r0 * Math.Sin(angle)), Width * 0.5f);
			}
			Gl.glEnd();
		}
	
		private float view_rotx = 20.0f, view_roty = 30.0f, view_rotz = 0.0f;
		private int gear1, gear2, gear3;
		private float angle = 0.0f;
	
		private int frames = 0;
	
		private void OnExpose (object o, ExposeEventArgs args)
		{
			if (!realized)
				return;
	
			/*** OpenGL BEGIN ***/
			m_gl.MakeCurrent ();
			
			Gl.glClear (Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
			
			Gl.glPushMatrix ();
			Gl.glRotatef (view_rotx, 1, 0, 0);
			Gl.glRotatef (view_roty, 0, 1, 0);
			Gl.glRotatef (view_rotz, 0, 0, 1);
			
			Gl.glPushMatrix ();
			    Gl.glTranslatef (-3.0f, -2.0f, 0.0f);
			    Gl.glRotatef (angle, 0, 0, 1);
			    Gl.glCallList (gear1);
			Gl.glPopMatrix ();
			
			Gl.glPushMatrix ();
			    Gl.glTranslatef (3.1f, -2.0f, 0.0f);
			    Gl.glRotatef ((float)(-2.0 * angle - 9.0), 0, 0, 1);
			    Gl.glCallList (gear2);
			Gl.glPopMatrix ();
			
			Gl.glPushMatrix ();
			    Gl.glTranslatef (-3.1f, 4.2f, 0.0f);
			    Gl.glRotatef ((float)(-2.0 * angle - 25.0), 0, 0, 1);
			    Gl.glCallList (gear3);
			Gl.glPopMatrix ();
			Gl.glPopMatrix ();
			
			/*** OpenGL END ***/
			m_gl.SwapBuffers ();
			
			frames++;
		}
	
		private bool OnIdle ()
		{		
			angle += 2.0f;
			/* Invalidate the whole window. */
			GdkWindow.InvalidateRect (Allocation, false);
			/* Update synchronously (fast). */
			if (AppMain.is_sync)
				GdkWindow.ProcessUpdates (false);
			return true;
		}
		
		private GLib.IdleHandler Idle = null;
		
		private void IdleAdd ()
		{
			if (Idle == null) {
				Idle = new GLib.IdleHandler (OnIdle);
				GLib.Idle.Add (Idle);
			}
		}

		private void IdleRemove ()
		{
			if (Idle != null) 
				GLib.Idle.Remove (Idle);
			Idle = null;
		}

		private void OnMap (object o, MapEventArgs args)
		{
			IdleAdd ();
		}
		
		private void OnUnmap (object o, UnmapEventArgs args)
		{
			IdleRemove ();
		}
		
		private void OnVisible (object o, VisibilityNotifyEventArgs args)
		{
			if (args.Event.State == Gdk.VisibilityState.FullyObscured)
				IdleRemove ();
			else
				IdleAdd ();
		}

		private bool OnTimer ()
		{
			double fps = frames / (s_timerPeriod / 1000.0);
			System.Console.WriteLine ("{0} frames in {1} seconds = {2} FPS", frames, s_timerPeriod / 1000, fps);
			frames = 0;
			return true;
		}
		
		private void	OnConfigure (object o, ConfigureEventArgs args)
		{
			if (!realized)
				return;
				
	  		float h = (float) Allocation.Height / (float) Allocation.Width;

			/*** OpenGL BEGIN ***/
			m_gl.MakeCurrent ();
			Gl.glViewport (0, 0, Allocation.Width, Allocation.Height);
			Gl.glMatrixMode (Gl.GL_PROJECTION);	
  			Gl.glLoadIdentity ();
	  		Gl.glFrustum (-1.0, 1.0, -h, h, 5.0, 60.0);
  			Gl.glMatrixMode (Gl.GL_MODELVIEW);
  			Gl.glLoadIdentity ();
  			Gl.glTranslatef (0, 0, -40.0f);
			/*** OpenGL END ***/
		}
		
	
		private GlWidget m_gl = null;
		private uint s_timerPeriod = 5000;			// in ms
		private bool realized = false;

		public GearsArea (GdkGL.Config glconfig) 
		{
			// Enable GL Widget and schedule context creating
			m_gl = new GlWidget (this, glconfig);

			// Events
			ExposeEvent += new ExposeEventHandler (OnExpose);
			ConfigureEvent += new ConfigureEventHandler (OnConfigure);
			Realized += new EventHandler (OnRealize);
			MapEvent += new MapEventHandler (OnMap);
			UnmapEvent += new UnmapEventHandler (OnUnmap);
			VisibilityNotifyEvent += new VisibilityNotifyEventHandler (OnVisible);
			AddEvents ((int)Gdk.EventMask.VisibilityNotifyMask);

			// Timers
			Timeout.Add (s_timerPeriod, new Function (OnTimer));
		}


		private void	OnRealize (object o, EventArgs args)
		{
	  		float[] pos = {5.0f, 5.0f, 10.0f, 0.0f};
	  		float[] red = {0.8f, 0.1f, 0.0f, 1.0f};
	  		float[] green = {0.0f, 0.8f, 0.2f, 1.0f};
	  		float[] blue = {0.2f, 0.2f, 1.0f, 1.0f};
	
			/*** OpenGL BEGIN ***/
			m_gl.MakeCurrent ();
		  	
			Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_POSITION, pos);
	  		Gl.glEnable(Gl.GL_CULL_FACE);
	  		Gl.glEnable(Gl.GL_LIGHTING);
	  		Gl.glEnable(Gl.GL_LIGHT0);
	  		Gl.glEnable(Gl.GL_DEPTH_TEST);
	
	  		/* make the gears */
	  		gear1 = Gl.glGenLists(1);
	  		Gl.glNewList (gear1, Gl.GL_COMPILE);
	  		Gl.glMaterialfv(Gl.GL_FRONT, Gl.GL_AMBIENT_AND_DIFFUSE, red);
	  		Gear(1.0f, 4.0f, 1.0f, 20, 0.7f);
	  		Gl.glEndList();
	
	  		gear2 = Gl.glGenLists(1);
	  		Gl.glNewList (gear2, Gl.GL_COMPILE);
	  		Gl.glMaterialfv (Gl.GL_FRONT, Gl.GL_AMBIENT_AND_DIFFUSE, green);
	  		Gear (0.5f, 2.0f, 2.0f, 10, 0.7f);
	  		Gl.glEndList ();
	
	  		gear3 = Gl.glGenLists(1);
	  		Gl.glNewList (gear3, Gl.GL_COMPILE);
	  		Gl.glMaterialfv (Gl.GL_FRONT, Gl.GL_AMBIENT_AND_DIFFUSE, blue);
	  		Gear (1.3f, 2.0f, 0.5f, 10, 0.7f);
	  		Gl.glEndList();
	
	  		Gl.glEnable(Gl.GL_NORMALIZE);
			/*** OpenGL END ***/
			
			realized = true;	
   			QueueResize();
		}
		
		
		/* change view angle, exit upon ESC */
		public void OnKeyPress (object o, KeyPressEventArgs args)
		{
			switch (args.Event.Key) {
			case Gdk.Key.z:
				view_rotz += 5.0f;
				break;
			case Gdk.Key.Z:
				view_rotz -= 5.0f;
				break;
			case Gdk.Key.Up:
				view_rotx += 5.0f;
				break;
			case Gdk.Key.Down:
				view_rotx -= 5.0f;
				break;
			case Gdk.Key.Left:
				view_roty += 5.0f;
				break;
			case Gdk.Key.Right:
				view_roty -= 5.0f;
				break;
			case Gdk.Key.Escape:
				Gtk.Application.Quit ();
				break;
			default:
				return;
			}
			GdkWindow.InvalidateRect (Allocation, false);
		}
	}
}


	class AppMain {
	
		public static bool is_sync = true;

		public static int Main (string[] args)
		{				
			/*
			* Init GTK.
			*/
			Gtk.Application.Init (/*"Gears3D", null*/);
			
			/*
			* Init GtkGLExt.
			*/
			GtkGL.Application.Init (ref args);
			
			/*
			* Command line options.
			*/
			for (int i = 0; i < args.Length; i++) 
			    if (args[i] ==  "--async")
			    	is_sync = false;
			
			/*
			* Configure OpenGL-capable visual.
			*/
			
			/* Try double-buffered visual */
			GdkGL.Config glconfig = new GdkGL.Config (GdkGL.ConfigMode.Rgb | GdkGL.ConfigMode.Depth | GdkGL.ConfigMode.Double);
			if (glconfig == null) {
	      		Console.WriteLine ("*** Cannot find the double-buffered visual.\n*** Trying single-buffered visual.");
			    /* Try single-buffered visual */
				glconfig = new GdkGL.Config (GdkGL.ConfigMode.Rgb | GdkGL.ConfigMode.Depth);
				if (glconfig == null) {
			          Console.WriteLine ("*** Cannot find any OpenGL-capable visual.");
			          return 1;
				} 
			}
			
			/*
			* Top-level window.
			*/
			Window window = new Window (WindowType.Toplevel);
			window.Title = "gears";
			
			/* Get automatically redrawn if any of their children changed allocation. */
			window.ReallocateRedraws = true;

			window.DeleteEvent += new DeleteEventHandler (Window_Delete);
			
			/*
			* VBox.
			*/
			VBox vbox = new VBox (false, 0);
			window.Add (vbox);
			vbox.Show ();

			/*
			* Drawing area for drawing OpenGL scene.
			*/
			/* Set OpenGL-capability to the widget. */
			GearsArea drawing_area = new GearsArea (glconfig);
			drawing_area.SetSizeRequest (300, 300);

			window.KeyPressEvent += new KeyPressEventHandler (drawing_area.OnKeyPress);
		
			vbox.PackStart (drawing_area, true, true, 0);
			drawing_area.Show ();
			
			/*
			* Simple quit button.
			*/
			Button button = new Button ("Quit");
			button.Clicked += new EventHandler (Button_Click);
			vbox.PackStart (button, false, false, 0);
			button.Show ();

			/*
			* Show window.
			*/
			window.Show ();		
			
			/*
			* Main loop.
			*/
			Gtk.Application.Run ();
			return 0;
		}
	
		
		static void Window_Delete (object obj, DeleteEventArgs args)
		{
			Gtk.Application.Quit ();
			args.RetVal = true;
		}
		static void Button_Click (object obj, EventArgs args)
		{
			Gtk.Application.Quit ();
		}

	
	}	



